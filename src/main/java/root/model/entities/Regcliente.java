/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author javi3
 */
@Entity
@Table(name = "regclientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regcliente.findAll", query = "SELECT r FROM Regcliente r"),
    @NamedQuery(name = "Regcliente.findByIdclientes", query = "SELECT r FROM Regcliente r WHERE r.idclientes = :idclientes"),
    @NamedQuery(name = "Regcliente.findByNombre", query = "SELECT r FROM Regcliente r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Regcliente.findByApellido1", query = "SELECT r FROM Regcliente r WHERE r.apellido1 = :apellido1"),
    @NamedQuery(name = "Regcliente.findByApellido2", query = "SELECT r FROM Regcliente r WHERE r.apellido2 = :apellido2"),
    @NamedQuery(name = "Regcliente.findByCorreo", query = "SELECT r FROM Regcliente r WHERE r.correo = :correo")})
public class Regcliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "idclientes")
    private String idclientes;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido1")
    private String apellido1;
    @Size(max = 2147483647)
    @Column(name = "apellido2")
    private String apellido2;
    @Size(max = 2147483647)
    @Column(name = "correo")
    private String correo;

    public Regcliente() {
    }

    public Regcliente(String idclientes) {
        this.idclientes = idclientes;
    }

    public String getIdclientes() {
        return idclientes;
    }

    public void setIdclientes(String idclientes) {
        this.idclientes = idclientes;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idclientes != null ? idclientes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regcliente)) {
            return false;
        }
        Regcliente other = (Regcliente) object;
        if ((this.idclientes == null && other.idclientes != null) || (this.idclientes != null && !this.idclientes.equals(other.idclientes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.Regcliente[ idclientes=" + idclientes + " ]";
    }
    
}
