/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.model.entities.Regcliente;

/**
 *
 * @author javi3
 */
public class RegclienteDAO implements Serializable {

    public RegclienteDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
   
    public RegclienteDAO() {   
    }
   private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
       
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Regcliente regcliente) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        System.out.print("proceso crear");
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(regcliente);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.print("Error en creacion "+ex.getMessage());
            if (findRegcliente(regcliente.getIdclientes()) != null) {
                throw new PreexistingEntityException("Regcliente " + regcliente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Regcliente regcliente) throws NonexistentEntityException, Exception {
        EntityManager em = null;
                    System.out.print("proceso edicion");

        try {
            em = getEntityManager();
            em.getTransaction().begin();
            regcliente = em.merge(regcliente);
            em.getTransaction().commit();
        } catch (Exception ex) {
                        System.out.print("Error en editar"+ex.getMessage());
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = regcliente.getIdclientes();
                if (findRegcliente(id) == null) {
                    throw new NonexistentEntityException("The regcliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Regcliente regcliente;
            try {
                regcliente = em.getReference(Regcliente.class, id);
                regcliente.getIdclientes();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The regcliente with id " + id + " no longer exists.", enfe);
            }
            em.remove(regcliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Regcliente> findRegclienteEntities() {
        return findRegclienteEntities(true, -1, -1);
    }

    public List<Regcliente> findRegclienteEntities(int maxResults, int firstResult) {
        return findRegclienteEntities(false, maxResults, firstResult);
    }

    private List<Regcliente> findRegclienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Regcliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Regcliente findRegcliente(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Regcliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegclienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Regcliente> rt = cq.from(Regcliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
