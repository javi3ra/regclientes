/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.RegclienteDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.entities.Regcliente;

/**
 *
 * @author javi3
 */
@WebServlet(name = "controllerList", urlPatterns = {"/controllerList"})
public class ControllerList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerList</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerList at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.print("Controlador listar");

//        String id = request.getParameter("id");
//        String nombre = request.getParameter("nombre");
//        String ap1 = request.getParameter("apellido1");
//        String ap2 = request.getParameter("apellido2");
//        String correo = request.getParameter("correo");
        String sel = request.getParameter("sel");
        String action1 = request.getParameter("opcion");

//        System.out.print("RUT" + id);
//        System.out.print("nombre" + nombre);
//        System.out.print("apellido Paterno" + ap1);
//        System.out.print("apellido" + ap2);
//        System.out.print("correo" + correo);
        System.out.print("Controlador Lista id selecionado " + sel);
        System.out.print("Controlador Lista opcion selecionado " + action1);

        
        if (action1.equalsIgnoreCase("editar")) {
            Regcliente mc = new Regcliente();
            RegclienteDAO dao = new RegclienteDAO();
            mc = dao.findRegcliente(sel);
            request.setAttribute("regclientes", mc);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
        }

        if (action1.equalsIgnoreCase("eliminar")) {

            RegclienteDAO dao = new RegclienteDAO();
            List<Regcliente> mc = new ArrayList<Regcliente>();
            try {
                dao.destroy(sel);
                mc = dao.findRegclienteEntities();

            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ControllerList.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("regclientes", mc);
            request.getRequestDispatcher("registro.jsp").forward(request, response);
        }

        if (action1.equalsIgnoreCase("listar")) {  
            
            List<Regcliente> mc = new ArrayList<Regcliente>();
             RegclienteDAO dao = new RegclienteDAO();
             mc= dao.findRegclienteEntities();
             request.setAttribute("regcliente", mc);
             request.getRequestDispatcher("registro.jsp").forward(request, response);
        }
        

        if (action1.equalsIgnoreCase("crear")) {
            request.getRequestDispatcher("nuevo.jsp").forward(request, response);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}
