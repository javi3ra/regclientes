/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.RegclienteDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.entities.Regcliente;

/**
 *
 * @author javi3
 */
@WebServlet(name = "controllerSave", urlPatterns = {"/controllerSave"})
public class ControllerSave extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerSave</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerSave at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.print("Controlador Guardar");

        String id = request.getParameter("id");
        String nombre = request.getParameter("nombre");
        String ap1 = request.getParameter("apellido1");
        String ap2 = request.getParameter("apellido2");
        String correo = request.getParameter("correo");
        String action = request.getParameter("opcion");

        System.out.println("RUT :" + id);
        System.out.println("nombre :" + nombre);
        System.out.println("apellido Paterno :" + ap1);
        System.out.println("apellido :" + ap2);
        System.out.println("correo :" + correo);

        if (action.equalsIgnoreCase("crear")) {
            try {
                RegclienteDAO dao = new RegclienteDAO();
                Regcliente cli = new Regcliente();
                cli.setIdclientes(id);
                cli.setNombre(nombre);
                cli.setApellido1(ap1);
                cli.setApellido2(ap2);
                cli.setCorreo(correo);
                dao.create(cli);

                List<Regcliente> mc = dao.findRegclienteEntities();

                System.out.println("Cantidad clientes en Base de Datos " + mc.size());
                request.setAttribute("regclientes", mc);
                request.getRequestDispatcher("registro.jsp").forward(request, response);

            } catch (Exception ex) {
               System.out.print("Error en crear "+ex.getMessage());

                Logger.getLogger(ControllerSave.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (action.equalsIgnoreCase("GuardarEditado")) {
            try {
                RegclienteDAO dao = new RegclienteDAO();
                Regcliente mc = new Regcliente();
                mc.setIdclientes(id);
                mc.setNombre(nombre);
                mc.setApellido1(ap1);
                mc.setApellido2(ap2);
                mc.setCorreo(correo);
                dao.edit(mc);

                List<Regcliente> ed = dao.findRegclienteEntities();
                System.out.print("Lista de clientes" + ed.size());
                request.setAttribute("regclientes", ed);
                request.getRequestDispatcher("registro.jsp").forward(request, response);

            } catch (Exception ex) {
                System.out.print("Error en edicion "+ex.getMessage());
                Logger.getLogger(ControllerList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (action.equalsIgnoreCase("Ver Registro")) {
            System.out.print("procesando...");
            List<Regcliente> mc = new ArrayList<Regcliente>();
            RegclienteDAO dao = new RegclienteDAO();
            mc = dao.findRegclienteEntities();
            request.setAttribute("regclientes", mc);

            request.getRequestDispatcher("registro.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
