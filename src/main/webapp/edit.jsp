<%-- 
    Document   : prueba
    Created on : 19-abr-2020, 17:43:40
    Author     : javi3
--%>

<%@page import="root.model.entities.Regcliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
  
<%  Regcliente client = (Regcliente) request.getAttribute("regclientes");%>

<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">         
        <link rel="stylesheet" href="styles.css">
        <title>R3GiStRo</title>
    </head>

    <body class="text-center" >
        <main role="main" class="inner cover">
            <h1 class="cover-heading" style="margin-bottom: 50px;margin-top: 50px;">Modifique</h1>
        </main>
        <form action="controllerSave" method="POST">
            <p> <h3 style="margin-right: 100px;margin-left: 100px;padding-right: 420px;"> <span class="badge badge-light">Datos</span></h3>
            
           <div class="form-row" style="margin-left: 360px;margin-right: 380px;margin-top: 20px;margin-bottom: 20px;">   
                <div class="form-group " style="margin-left: 20px;margin-right: 20px;">
                    <label for="inputRUT">RUT Cliente</label>
                    <input type="text" class="form-control" name="id" value="<%= client.getIdclientes() %>" id="id" placeholder="ingrese datos">
                </div>

                <div class="form-group ">
                    <label for="inputNombre">Nombre Cliente</label>
                    <input type="text" class="form-control" name="nombre" value="<%= client.getNombre() %>" id="nombre" placeholder="ingrese datos">
                </div>
            </div>

            <div class="form-row" style="margin-left: 360px;margin-right: 380px;margin-top: 20px;margin-bottom: 20px;">   
                 <div class="form-group " style="margin-left: 20px;margin-right: 20px;">
                    <label for="inputAp1">Apellido Paterno</label>
                    <input type="text" class="form-control" name="apellido1" value="<%= client.getApellido1() %>" id="ap1"  placeholder="ingrese datos"/>
                </div>
                <div class="form-group">
                    <label for="inputAp2">Apellido Materno</label>
                    <input type="text" class="form-control" name="apellido2" value="<%= client.getApellido2() %>" id="ap2"  placeholder="ingrese datos"/>
                </div>
            </div>

                <p> <h3 style="margin-right: 100px;margin-left: 100px;padding-right: 420px;"> <span class="badge badge-light">Contacto</span></h3>
            
            <div class="form-row" style="margin-left: 360px;margin-right: 380px;margin-top: 20px;margin-bottom: 20px;">
                <div class="form-group " style="margin-left: 20px;margin-right: 20px;">
                    <label for="inputCorreo">Email</label>
                    <input type="text" class="form-control" name="correo" value="<%= client.getCorreo() %>" id="correo" placeholder="example@exam.exa"/>
                </div>
            </div>

            <p><button class="btn btn-info" type="submit" name="opcion" value="GuardarEditado" />Guardar</button>
               



        </form>
    </body>
</html>