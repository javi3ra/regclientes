<%-- 
    Document   : index
    Created on : 19-abr-2020, 0:23:01
    Author     : javi3
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">         
        <link rel="stylesheet" href="styles.css">
        <title>R3GiStRo</title>
    </head>
    <div id="body">
        <body class="text-center"  >
            <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">       
                <main role="main" class="inner cover">
                    <h1 class="cover-heading" style="margin-bottom: 50px;margin-top: 50px;">Registro de clientes!</h1>
                    <form action="controller" method="POST">
                            <input class="btn btn-light" type="submit" name="accion" value="Ver Registro" style="margin-right: 15px;margin-left: 15px;">      
                   
                    </form>
                </main>

                <footer class="mastfoot mt-auto">
                    <div class="inner" style="margin-top: 50px;">
                        <p>Taller de Aplicaciones Empresariales</p>
                    </div>
                </footer>
            </div>

        </body>
    </div>
</html>
