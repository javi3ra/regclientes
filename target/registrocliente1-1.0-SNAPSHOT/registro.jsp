
<%-- 
    Document   : lista
    Created on : 19-abr-2020, 0:24:58
    Author     : javi3
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.model.entities.Regcliente"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Regcliente> client = (List<Regcliente>) request.getAttribute("regclientes");
    Iterator<Regcliente> itclient = client.iterator();
%>    
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">         
        <link rel="stylesheet" href="styles.css">
        <title>R3GiStRo</title>
    </head>
    <body class="center">
         <main role="main" class="inner cover">
                <h1 class="cover-heading" style="margin-bottom: 50px;margin-top: 50px;"><b>Registro de clientes!</b></h1>
            </main>     
        <h3> <span class="badge badge-light">Lista</span></h3> 
        
        <form action="controllerList" method="POST">
       <button class="btn btn-info" type="submit" name="opcion" value="crear"/><b>crear</b></button>
        <button class="btn btn-light" type="submit" name="opcion" value="listar"/><b>actualizar</b></button>  
        </form>
        
        <form action="controllerList" method="POST">              
            <table border="1" class="table table-striped table-dark">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">RUT Cliente</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido Materno</th>
                        <th scope="col">Apellido Paterno</th>
                        <th scope="col">Correo</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                </thead>
                <tbody>

                    <% while (itclient.hasNext()) {
                            Regcliente ob = itclient.next();%> 
                    <tr>
                        <td> <input type="radio" name="sel" value="<%= ob.getIdclientes()%>" required/></td>
                        <td scope="row"><%= ob.getIdclientes()%></td>
                        <td><%= ob.getNombre()%></td>
                        <td><%= ob.getApellido1()%></td>
                        <td><%= ob.getApellido2()%></td>
                        <td><%= ob.getCorreo()%></td>
                        <td> 
                            <div class="input-group-append" id="button-addon4">
                                <button class="btn btn-outline-secondary" type="submit" name="opcion" value="editar" />editar</button>
                                <button class="btn btn-outline-secondary" type="submit" name="opcion" value="eliminar" />eliminar</button>                           
                            </div>
                        </td> 
                    </tr>
                    <%}%>
                </tbody>
            </table>
         </form>
    </body>
</html>